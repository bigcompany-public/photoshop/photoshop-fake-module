from PyPDF2 import PdfReader, PageObject
from tabula import read_pdf
import json
import re
import os
import shutil
import logging
from pathlib import Path
import argparse
from pandas import DataFrame
from typing import List

class DocumentationToModule():
    """
    This class is designed to extract the content of adobe's Photoshop VBS Reference to generate a
    python module describing the Photoshop Object Model's classes and their methods/attributes.
    The methods and attributes will of course be empty, the goal here is just to provide a fake
    module for type hinting and autocompletion

    The classes' names are extracted from the raw text of each page, and the methods/properties are
    extracted from the tables.
    """
    def __init__(self, path:Path, first_page:int, last_page:int) -> None:
        self.path = Path(path)
        self.pdf_reader = reader = PdfReader(path)
        self.first_page = first_page
        self.last_page = last_page or len(reader.pages)
        self.classes_dict = {}
        self.constants_dict = {}
        self.events_dict = {}
        self.current_class_name:str = None
    
    def main(self):
        """Main function of the class. Generates a python module based on the pdf documentation"""
        pages = self.pdf_reader.pages[self.first_page-1:self.last_page]
        for i, page in enumerate(pages, self.first_page):
            self.extract_page(page, i)
        self.write_classes_json()
        self.write_constants_json()
        self.write_events_json()
        self.generate_py_package()
    
    def extract_page(self, page:PageObject, page_number:int):
        """Extracts the text/table data from the pdf page"""
        logging.info(f"Reading page {page_number}")
        text = page.extract_text()
        dataframes = read_pdf(self.path, pages=page_number, pandas_options={'dtype': str}, lattice=True)
        # Replace 'nan' by None
        dataframes = [df.where(df.notnull(), None) for df in dataframes]
        dataframes = [df.replace("\r", " ", regex=True) for df in dataframes]
        self.update_current_class(text, dataframes)

        for dataframe in dataframes:
            dataframe = dataframe.where(dataframe.notnull(), None) 
            self.extract_properties(dataframe)
            self.extract_methods(dataframe)
            self.extract_scripting_constants(dataframe)
            self.extract_event_codes(dataframe)
    
    def update_current_class(self, text:str, dataframes:List[DataFrame]):
        """
        In adobe's pdf, a new class documentation always starts at the top of a page.
        Though, the first word of a page may also be part of a dataframe ()
        """
        # Pages always start with a header, which is not useful for our purpose
        first_line = text.split("\n")[2]

        # Get first item of any table
        first_item = None
        if dataframes:
            first_item = dataframes[0].iloc[0, 0].replace("\r", " ").split(" ")[0]
        
        # Classes name only contains letters, if anything else is found, it is probably just text
        pattern = re.compile("^[a-zA-Z]+$")
        if not pattern.match(first_line):
            return
        
        # "Properties" and "Methods" are obviously tables columns, skip them...
        if first_line in ["Properties", "Methods"]:
            return
        
        # The class name cannot be within a table
        if first_line == first_item:
            return
        
        logging.info(f"New class detected : {first_line}")
        self.current_class_name = first_line
        class_docstring = self.get_class_docstring(text)
        self.classes_dict[first_line] = {"docstring" : class_docstring, "properties" : [], "methods" : []}

    def get_class_docstring(self, text:str):
        """
        Get the docstring of the class
        The docstring is after the header and the class name and stops when a table is encountered
        """
        class_docstring = ""
        for line in text.split("\n")[3:]:
            stop_at = ["Properties", "Property", "Methods", "Method"]
            if any([line.startswith(string) for string in stop_at]):
                break
            class_docstring += f"{line}"
            class_docstring = class_docstring.replace("  ", " ").replace(" .", ".")
        return class_docstring

    def extract_properties(self, dataframe:DataFrame):
        if dataframe.columns[0] != "Property":
            return
    
        for i in range(len(dataframe)):
            property_name = str(dataframe.iloc[i, 0]).split(" ")[0].strip()
            if property_name == "None":
                continue
            property_type = str(dataframe.iloc[i, 1]).strip()
            property_doctring = str(dataframe.iloc[i, 2]).strip()

            # Format type
            property_type = self.format_type_hint(property_type)

            self.classes_dict[self.current_class_name]["properties"].append(
                {
                    "name" : property_name,
                    "type" : property_type,
                    "docstring" : property_doctring
                }
            )
    
    def format_type_hint(self, type_hint:str):
        # Super specific cases
        if type_hint == "PsLayerCompressionTy pe":
            return "PsLayerCompressionType"
        if type_hint in ["Array","array"]:
            return "list"
        if type_hint == "Array of256 Numbers (Long)":
            return "List[int]"
        if type_hint == "Array of 256 Numbers (Long)":
            return "List[int]"
        if type_hint == "Array of ArtLayer and/or LayerSet":
            return "List[ArtLayer|LayerSet]"
        if type_hint == "array of twenty-five Numbers Long":
            return "List[int]"
        if type_hint == "array Double":
            return "List[float]"
        if type_hint == "open options":
            return "Any"
        if type_hint == "corresponding SaveOptions":
            return "Any"
        if type_hint == "Array of Arrays: Array(Array (tag, tag data)), ...)":
            return "list"
        if type_hint == "Array (Strings)":
            return "List[str]"
        if type_hint == "Array(Double)":
            return "List[float]"
        if type_hint == "array SubPathInfo s":
            return "List[SubPathInfo]"
        if type_hint == "Array of Number(Double)":
            return "List[float]"
        if type_hint == "Nothing":
            return "None"
        
        

        type_hint = re.sub(r"Number \(Long\)", "int", type_hint, flags=re.IGNORECASE)
        type_hint = re.sub(r"Number \(Double\)", "float", type_hint, flags=re.IGNORECASE)
        pattern = r"array(?: of)? \(([a-zA-Z]+)\s"
        match = re.search(pattern, type_hint, flags=re.IGNORECASE)
        if match:
            type_hint = f"List[{match.group(1)}]"
        
        pattern = r"array of ([a-zA-Z]+)"
        match = re.search(pattern, type_hint, flags=re.IGNORECASE)
        if match:
            match = match.group(1)
            match = "str" if match in ["string", "String"] else match
            type_hint = f"List[{match}]"
        
        pattern = r"array \( ([a-zA-Z]+)\s"
        match = re.search(pattern, type_hint, flags=re.IGNORECASE)
        if match:
            type_hint = f"List[{match.group(1)}]"
        
        pattern = r"object \(([a-zA-Z ]+)"
        match = re.search(pattern, type_hint, flags=re.IGNORECASE)
        if match:
            type_hint = match.group(1)
        
        type_hint = type_hint.replace(" or ", "|")
        type_hint = type_hint.replace(" and/or ", "|")
        if type_hint in ["string", "String"]:
            type_hint = "str"
        if type_hint in ["boolean", "Boolean"]:
            type_hint = "bool"
        if type_hint in ["file", "File"]:
            type_hint = "str"
        if type_hint in ["none", "None"]:
            type_hint = "None"
        
        
        
        return type_hint


    def extract_methods(self, dataframe:DataFrame):
        if dataframe.columns[0] != "Method":
            return
    
        for typ in range(len(dataframe)):
            method_name = str(dataframe.iloc[typ, 0]).split("(")[0].strip()
            if method_name == "None":
                continue
            method_params = str(dataframe.iloc[typ, 0]).split("(")[-1].split(")")[0].split("Note")[0].split(",")
            method_params_typing = str(dataframe.iloc[typ, 1]).split("Note")[0].split("Examples")[0].strip()
            method_rtype = str(dataframe.iloc[typ, 2]).strip()
            method_doctring = str(dataframe.iloc[typ, 3]).strip()

            # Format params
            method_params = [i for i in method_params if i and i != "None"]
            method_params = [i.replace("[", "") for i in method_params]
            method_params = [i.replace(" ", "") for i in method_params]
            method_params = [i.replace("]", "(Optional)") for i in method_params]
            method_params = [i for i in method_params if i]

            # Format params typing
            method_params_typing = method_params_typing.replace("Object (SolidColor, or HistoryState)", "List[SolidColor|HistoryState],")
            method_params_typing = method_params_typing.replace("PsColorBlendMode", "PsColorBlendMode,")
            method_params_typing = method_params_typing.replace("*", "")
            method_params_typing = method_params_typing.replace("array", ",array")
            method_params_typing = method_params_typing.replace("Array", ",array")
            method_params_typing = method_params_typing.replace("Number (Long)", "int,")
            method_params_typing = method_params_typing.replace("Number (Double)", "float,")
            method_params_typing = method_params_typing.replace("Number(Double)", "float,")
            method_params_typing = method_params_typing.replace("Boolean", "bool,")
            method_params_typing = method_params_typing.replace("boolean", "bool,")
            method_params_typing = method_params_typing.replace("String", "str,")
            method_params_typing = method_params_typing.replace("string", "str,")
            method_params_typing = method_params_typing.replace("array of strings", "List[str]")
            method_params_typing = method_params_typing.replace("Array of str", "List[str]")
            method_params_typing = method_params_typing.replace("Array of float", "List[float]")
            method_params_typing = method_params_typing.replace("Array of int", "List[int]")
            method_params_typing = method_params_typing.replace("Object", "")
            method_params_typing = method_params_typing.replace("object", "")
            method_params_typing = method_params_typing.replace("(", "")
            method_params_typing = method_params_typing.replace(")", ",")
            method_params_typing = method_params_typing.replace(" Ps", ", Ps")
            method_params_typing = method_params_typing.split(",")

            method_params_typing = [i for i in method_params_typing if i and i != "None"]
            method_params_typing = [i.strip() for i in method_params_typing]
            method_params_typing = [i for i in method_params_typing if i]
            temp = []

            for typ in method_params_typing:
                typ = self.format_type_hint(typ)
                temp.append(typ)
            method_params_typing = temp
            
            # Format rtype
            method_rtype = self.format_type_hint(method_rtype)

            
            self.classes_dict[self.current_class_name]["methods"].append(
                {
                    "name" : method_name,
                    "params" : method_params,
                    "params_typing" : method_params_typing,
                    "rtype" : method_rtype,
                    "docstring" : method_doctring
                }
            )
    
    def extract_scripting_constants(self, dataframe:DataFrame):
        if dataframe.columns[0] != "Constant type":
            return
    
        for i in range(len(dataframe)):
            constant_name = str(dataframe.iloc[i, 0]).strip()
            docstring = str(dataframe.iloc[i, 2]).strip()
            self.constants_dict[constant_name] = {
                "docstring" : docstring,
                "values" : {}
            }

            options = str(dataframe.iloc[i, 1]).split(")")
            options = [opt for opt in options if opt]
            for option in options:
                option = option.split("(")
                if not len(option) == 2:
                    continue
                key = option[1].strip()
                value = option[0].strip()
                self.constants_dict[constant_name]["values"][key] = value
    
    def extract_event_codes(self, dataframe:DataFrame):
        if dataframe.columns[0] != "Event":
            return
        
        for i in range(len(dataframe)):
            event_name = str(dataframe.iloc[i, 0]).strip()
            event_id = str(dataframe.iloc[i, 1]).replace("'", "").replace("\"", "").strip()
            self.events_dict[event_name] = event_id

    @property
    def classes_json(self) -> Path:
        return Path(__file__).parent.joinpath("classes.json")

    @property
    def constants_json(self) -> Path:
        return Path(__file__).parent.joinpath("constants.json")

    @property
    def events_json(self) -> Path:
        return Path(__file__).parent.joinpath("events.json")

    @property
    def package_path(self) -> Path:
        return Path(__file__).parent.joinpath("photoshop_object_model")
    
    def write_classes_json(self):
        if self.classes_json.exists():
            os.remove(self.classes_json)
        with self.classes_json.open('w') as f:
            f.write(json.dumps(self.classes_dict, indent=4))
    
    def write_constants_json(self):
        if self.constants_json.exists():
            os.remove(self.constants_json)
        with self.constants_json.open('w') as f:
            f.write(json.dumps(self.constants_dict, indent=4))
    
    def write_events_json(self):
        if self.events_json.exists():
            os.remove(self.events_json)
        with self.events_json.open('w') as f:
            f.write(json.dumps(self.events_dict, indent=4))

    def generate_py_package(self):
        """Generates classes, constants, and event id modules"""
        if self.package_path.exists():
            shutil.rmtree(self.package_path)
        self.package_path.mkdir()

        self.generate_classes_modules()
        self.generate_constants_module()
        self.generate_events_module()
        self.generate_init_file()
    
    def generate_classes_modules(self):
        """Generates the py package from the classes found in the documentation"""
        with self.classes_json.open("r") as f:
            classes_dict:dict = json.loads(f.read())
        
        for class_name, class_data in classes_dict.items():
            imports = set()
            imports_typing = set()
            class_text = f"class {class_name}():\n"
            class_text += '    """\n'
            class_text += f'    {class_data["docstring"]}\n'
            class_text += '    """\n'

            for prop in class_data["properties"]:
                typ = prop['type']
                class_text += "    @property\n"
                class_text += f"    def {prop['name']}(self)"
                if typ != class_name:
                    class_text += f" -> {typ}:\n"
                else:
                    class_text += ":\n"
                for subtyp in typ.split("|"):
                    if not subtyp in ["str", "float", "int", "bool", "List[str]", "List[float]", "List[int]", "list"]:
                        _subtyp = subtyp.replace("List[", "").replace("]", "")
                        imports.add(_subtyp)
                    if "List" in subtyp:
                        imports_typing.add("List")
                    if "Any" in subtyp:
                        imports_typing.add("Any")
                class_text += f'        """\n'
                class_text += f'        {prop["docstring"]}\n'
                class_text += f'        """\n'
                class_text += f'        ...\n\n'
            
            for method in class_data["methods"]:
                class_text += f"    def {method['name']}(self"
                param:str
                for i, param in enumerate(method["params"]):
                    param_name = param.replace('(Optional)', '')
                    class_text += f", {param_name}"
                    if len(method["params"]) == len(method["params_typing"]):
                        typ = method['params_typing'][i]
                        if typ != class_name:
                            class_text += f":{typ}"
                            if not typ in ["str", "float", "int", "bool", "List[str]", "List[float]", "List[int]", "None", "List", "Any"]:
                                typ = typ.replace("List[", "").replace("]", "")
                                imports.add(typ)
                
                rtype = method["rtype"]
                if rtype != class_name:
                    class_text += f') -> {rtype}:\n'
                    if not rtype in ["str", "float", "int", "bool", "List[str]", "List[float]", "List[int]", "None"]:
                        rtype = rtype.replace("List[", "").replace("]", "")
                        imports.add(rtype) 
                else:
                    class_text += f'):\n'
                class_text += f'        """\n'
                class_text += f'        {method["docstring"]}\n'
                class_text += f'        """\n'
                class_text += f'        ...\n\n'

            import_block = """
from __future__ import annotations
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from events import *
    from scripting_constants import *
"""
            imp:str
            sub_imports = set()
            for imp in imports:
                for sub_imp in imp.split("|"):
                    sub_imports.add(sub_imp)

            import_block += f"    from typing import List, Any\n"
            for imp in sub_imports:
                if imp in ["list"]:
                    continue
                if imp.startswith("Ps"): # Scripting constant:
                    continue
                import_block += f"    from .{imp.lower()} import {imp}\n"
            class_text = import_block + "\n" + class_text

            module_path = self.package_path.joinpath(class_name.lower() + ".py")
            with module_path.open("w", encoding="utf-8", errors="ignore") as f:
                f.write(class_text)

    def generate_constants_module(self):
        """Generates the py package from the constants found in the documentation"""
        with self.constants_json.open("r") as f:
            constants_dict:dict = json.loads(f.read())
        
        constant_text = ""
        for constant_name, constant_data in constants_dict.items():
            constant_text += f"class {constant_name}():\n"
            constant_text += '    """\n'
            constant_text += f'    {constant_data["docstring"]}\n'
            constant_text += '    """\n'

            for key, value in constant_data["values"].items():
                constant_text += f"    {key} = {value}\n"
            constant_text += "\n"
            
        module_path = self.package_path.joinpath("scripting_constants.py")
        with module_path.open("w", encoding="utf-8", errors="ignore") as f:
            f.write(constant_text)
    
    def generate_events_module(self):
        with self.events_json.open("r") as f:
            events_dict:dict = json.loads(f.read())
        
        events_text = """
class Events():
    def __init__(self):
"""
        for key, value in events_dict.items():
            if key[0].isdigit():
                key = f"_{key}"
            events_text += f"        self.{key} = \"{value}\"\n"
        
        module_path = self.package_path.joinpath("events.py")
        with module_path.open("w", encoding="utf-8", errors="ignore") as f:
            f.write(events_text)
    
    def generate_init_file(self):
        with self.classes_json.open("r") as f:
            classes_dict:dict = json.loads(f.read())
        with self.constants_json.open("r") as f:
            constants_dict:dict = json.loads(f.read())

        init_file = self.package_path.joinpath("__init__.py")
        text = ""
        
        for class_name in classes_dict:
            text += f"from .{class_name.lower()} import {class_name}\n"
        for constant_name in constants_dict:
            text += f"from .scripting_constants import {constant_name}\n"
        text += f"from .events import Events\n"

        text += """
class PhotoshopObjectModel():
    def __init__(self):
"""
        for class_name in classes_dict:
            text += f"        self.{class_name} = {class_name}()\n"
        for constant_name in constants_dict:
            text += f"        self.{constant_name} = {constant_name}()\n"
        text += f"        self.Events = Events()\n"

        with init_file.open("w") as f:
            f.write(text)
        



if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--path", required=True, help="Path to the pdf file to convert to a module")
    parser.add_argument("-fp", "--first_page", type=int, required=False, default=8)
    parser.add_argument("-lp", "--last_page", type=int, required=False)
    args = parser.parse_args()

    doc = DocumentationToModule(args.path, args.first_page, args.last_page)
    # doc.main()
    doc.generate_py_package()
    from photoshop_object_model import *
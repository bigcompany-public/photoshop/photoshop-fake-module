from .actiondescriptor import ActionDescriptor
from .actionlist import ActionList
from .actionreference import ActionReference
from .application import Application
from .artlayer import ArtLayer
from .artlayers import ArtLayers
from .batchoptions import BatchOptions
from .bitmapconversionoptions import BitmapConversionOptions
from .bmpsaveoptions import BMPSaveOptions
from .camerarawopenoptions import CameraRAWOpenOptions
from .channel import Channel
from .channels import Channels
from .cmykcolor import CMYKColor
from .colorsampler import ColorSampler
from .colorsamplers import ColorSamplers
from .contactsheetoptions import ContactSheetOptions
from .countitem import CountItem
from .countitems import CountItems
from .dicomopenoptions import DICOMOpenOptions
from .document import Document
from .documentinfo import DocumentInfo
from .documents import Documents
from .epsopenoptions import EPSOpenOptions
from .epssaveoptions import EPSSaveOptions
from .exportoptionsillustrator import ExportOptionsIllustrator
from .exportoptionssaveforweb import ExportOptionsSaveForWeb
from .gallerybanneroptions import GalleryBannerOptions
from .gallerycustomcoloroptions import GalleryCustomColorOptions
from .galleryimagesoptions import GalleryImagesOptions
from .galleryoptions import GalleryOptions
from .gallerysecurityoptions import GallerySecurityOptions
from .gallerythumbnailoptions import GalleryThumbnailOptions
from .gifsaveoptions import GIFSaveOptions
from .graycolor import GrayColor
from .historystate import HistoryState
from .historystates import HistoryStates
from .hsbcolor import HSBColor
from .indexedconversionoptions import IndexedConversionOptions
from .jpegsaveoptions import JPEGSaveOptions
from .labcolor import LabColor
from .layercomp import LayerComp
from .layercomps import LayerComps
from .layers import Layers
from .layerset import LayerSet
from .layersets import LayerSets
from .measurementlog import MeasurementLog
from .measurementscale import MeasurementScale
from .nocolor import NoColor
from .notifier import Notifier
from .notifiers import Notifiers
from .pathitem import PathItem
from .pathitems import PathItems
from .pathpoint import PathPoint
from .pathpointinfo import PathPointInfo
from .pathpoints import PathPoints
from .pdfopenoptions import PDFOpenOptions
from .pdfsaveoptions import PDFSaveOptions
from .photocdopenoptions import PhotoCDOpenOptions
from .photoshopsaveoptions import PhotoshopSaveOptions
from .pictfilesaveoptions import PICTFileSaveOptions
from .picturepackageoptions import PicturePackageOptions
from .pixarsaveoptions import PixarSaveOptions
from .pngsaveoptions import PNGSaveOptions
from .preferences import Preferences
from .presentationoptions import PresentationOptions
from .rawformatopenoptions import RawFormatOpenOptions
from .rawsaveoptions import RawSaveOptions
from .rgbcolor import RGBColor
from .selection import Selection
from .sgirgbsaveoptions import SGIRGBSaveOptions
from .solidcolor import SolidColor
from .subpathinfo import SubPathInfo
from .subpathitem import SubPathItem
from .subpathitems import SubPathItems
from .targasaveoptions import TargaSaveOptions
from .textfont import TextFont
from .textfonts import TextFonts
from .textitem import TextItem
from .tiffsaveoptions import TiffSaveOptions
from .xmpmetadata import XMPMetadata
from .scripting_constants import PsAdjustmentReference
from .scripting_constants import PsAnchorPosition
from .scripting_constants import PsAntiAlias
from .scripting_constants import PsAutoKernType
from .scripting_constants import PsBatchDestinationType
from .scripting_constants import PsBitmapConversionType
from .scripting_constants import PsBitmapHalfToneType
from .scripting_constants import PsBitsPerChannelType
from .scripting_constants import PsBlendMode
from .scripting_constants import PsBMPDepthType
from .scripting_constants import PsByteOrder
from .scripting_constants import PsCameraRAWSettingsType
from .scripting_constants import PsCameraRAWSize
from .scripting_constants import PsCase
from .scripting_constants import PsChangeMode
from .scripting_constants import PsChannelType
from .scripting_constants import PsColorBlendMode
from .scripting_constants import PsColorModel
from .scripting_constants import PsColorPicker
from .scripting_constants import PsColorProfileType
from .scripting_constants import PsColorReductionType
from .scripting_constants import PsColorSpaceType
from .scripting_constants import PsCopyrightedType
from .scripting_constants import PsCreateFields
from .scripting_constants import PsCropToType
from .scripting_constants import PsDCSType
from .scripting_constants import PsDepthMapSource
from .scripting_constants import PsDescValueType
from .scripting_constants import PsDialogModes
from .scripting_constants import PsDirection
from .scripting_constants import PsDisplacementMapType
from .scripting_constants import PsDitherType
from .scripting_constants import PsDocumentFill
from .scripting_constants import PsDocumentMode
from .scripting_constants import PsEditLogItemsType
from .scripting_constants import PsElementPlacement
from .scripting_constants import PsEliminateFields
from .scripting_constants import PsExportType
from .scripting_constants import PsExtensionType
from .scripting_constants import PsFileNamingType
from .scripting_constants import psFontPreviewType
from .scripting_constants import PsForcedColors
from .scripting_constants import PsFormatOptionsType
from .scripting_constants import PsGalleryConstrainType
from .scripting_constants import PsGalleryFontType
from .scripting_constants import PsGallerySecurityTextPositionType
from .scripting_constants import PsGallerySecurityTextRotateType
from .scripting_constants import PsGallerySecurityType
from .scripting_constants import PsGalleryThumbSizeType
from .scripting_constants import PsGeometry
from .scripting_constants import PsGridLineStyle
from .scripting_constants import PsGridSize
from .scripting_constants import PsGuideLineStyle
from .scripting_constants import PsIllustratorPathType
from .scripting_constants import PsIntent
from .scripting_constants import PsJavaScriptExecutionMode
from .scripting_constants import PsJustification
from .scripting_constants import PsLanguage
from .scripting_constants import PsLayerCompressionType
from .scripting_constants import PsLayerKind
from .scripting_constants import PsLayerType
from .scripting_constants import PsLensType
from .scripting_constants import PsMagnificationType
from .scripting_constants import PsMatteType
from .scripting_constants import PsMeasurementRange
from .scripting_constants import PsMeasurementSource
from .scripting_constants import PsNewDocumentMode
from .scripting_constants import PsNoiseDistribution
from .scripting_constants import PsOffsetUndefinedAreas
from .scripting_constants import PsOpenDocumentMode
from .scripting_constants import PsOpenDocumentType
from .scripting_constants import PsOperatingSystem
from .scripting_constants import PsOrientation
from .scripting_constants import PsOtherPaintingCursors
from .scripting_constants import PsPaintingCursors
from .scripting_constants import PsPaletteType
from .scripting_constants import PsPathKind
from .scripting_constants import PsPDFCompatibilityType
from .scripting_constants import PsPDFEncoding
from .scripting_constants import PsPDFResampleType
from .scripting_constants import PsPDFStandardType
from .scripting_constants import PsPhotoCDColorSpace
from .scripting_constants import PsPhotoCDSize
from .scripting_constants import PsPICTBitsPerPixels
from .scripting_constants import PsPICTCompression
from .scripting_constants import PsPicturePackageTextType
from .scripting_constants import PsPointKind
from .scripting_constants import PsPointType
from .scripting_constants import PsPolarConversionType
from .scripting_constants import PsPreviewType
from .scripting_constants import PsPurgeTarget
from .scripting_constants import PsQueryStateType
from .scripting_constants import PsRadialBlurMethod
from .scripting_constants import PsRadialBlurQuality
from .scripting_constants import PsRasterizeType
from .scripting_constants import PsReferenceFormType
from .scripting_constants import PsResampleMethod
from .scripting_constants import PsRippleSize
from .scripting_constants import PsSaveBehavior
from .scripting_constants import PsSaveDocumentType
from .scripting_constants import PsSaveEncoding
from .scripting_constants import PsSaveLogItemsType
from .scripting_constants import PsSaveOptions
from .scripting_constants import PsSelectionType
from .scripting_constants import PsShapeOperation
from .scripting_constants import PsSmartBlurMode
from .scripting_constants import PsSmartBlurQuality
from .scripting_constants import PsSourceSpaceType
from .scripting_constants import PsSpherizeMode
from .scripting_constants import PsStrikeThruType
from .scripting_constants import PsStrokeLocation
from .scripting_constants import PsTargaBitsPerPixels
from .scripting_constants import PsTextComposer
from .scripting_constants import PsTextType
from .scripting_constants import PsTextureType
from .scripting_constants import PsTIFFEncodingType
from .scripting_constants import PsToolType
from .scripting_constants import PsTransitionType
from .scripting_constants import PsTrimType
from .scripting_constants import PsTypeUnits
from .scripting_constants import PsUndefinedAreas
from .scripting_constants import PsUnderlineType
from .scripting_constants import PsUnits
from .scripting_constants import PsUrgency
from .scripting_constants import PsWarpStyle
from .scripting_constants import PsWaveType
from .scripting_constants import PsWhiteBalanceType
from .scripting_constants import PsZigZagType
from .events import Events

class PhotoshopObjectModel():
    def __init__(self):
        self.ActionDescriptor = ActionDescriptor()
        self.ActionList = ActionList()
        self.ActionReference = ActionReference()
        self.Application = Application()
        self.ArtLayer = ArtLayer()
        self.ArtLayers = ArtLayers()
        self.BatchOptions = BatchOptions()
        self.BitmapConversionOptions = BitmapConversionOptions()
        self.BMPSaveOptions = BMPSaveOptions()
        self.CameraRAWOpenOptions = CameraRAWOpenOptions()
        self.Channel = Channel()
        self.Channels = Channels()
        self.CMYKColor = CMYKColor()
        self.ColorSampler = ColorSampler()
        self.ColorSamplers = ColorSamplers()
        self.ContactSheetOptions = ContactSheetOptions()
        self.CountItem = CountItem()
        self.CountItems = CountItems()
        self.DICOMOpenOptions = DICOMOpenOptions()
        self.Document = Document()
        self.DocumentInfo = DocumentInfo()
        self.Documents = Documents()
        self.EPSOpenOptions = EPSOpenOptions()
        self.EPSSaveOptions = EPSSaveOptions()
        self.ExportOptionsIllustrator = ExportOptionsIllustrator()
        self.ExportOptionsSaveForWeb = ExportOptionsSaveForWeb()
        self.GalleryBannerOptions = GalleryBannerOptions()
        self.GalleryCustomColorOptions = GalleryCustomColorOptions()
        self.GalleryImagesOptions = GalleryImagesOptions()
        self.GalleryOptions = GalleryOptions()
        self.GallerySecurityOptions = GallerySecurityOptions()
        self.GalleryThumbnailOptions = GalleryThumbnailOptions()
        self.GIFSaveOptions = GIFSaveOptions()
        self.GrayColor = GrayColor()
        self.HistoryState = HistoryState()
        self.HistoryStates = HistoryStates()
        self.HSBColor = HSBColor()
        self.IndexedConversionOptions = IndexedConversionOptions()
        self.JPEGSaveOptions = JPEGSaveOptions()
        self.LabColor = LabColor()
        self.LayerComp = LayerComp()
        self.LayerComps = LayerComps()
        self.Layers = Layers()
        self.LayerSet = LayerSet()
        self.LayerSets = LayerSets()
        self.MeasurementLog = MeasurementLog()
        self.MeasurementScale = MeasurementScale()
        self.NoColor = NoColor()
        self.Notifier = Notifier()
        self.Notifiers = Notifiers()
        self.PathItem = PathItem()
        self.PathItems = PathItems()
        self.PathPoint = PathPoint()
        self.PathPointInfo = PathPointInfo()
        self.PathPoints = PathPoints()
        self.PDFOpenOptions = PDFOpenOptions()
        self.PDFSaveOptions = PDFSaveOptions()
        self.PhotoCDOpenOptions = PhotoCDOpenOptions()
        self.PhotoshopSaveOptions = PhotoshopSaveOptions()
        self.PICTFileSaveOptions = PICTFileSaveOptions()
        self.PicturePackageOptions = PicturePackageOptions()
        self.PixarSaveOptions = PixarSaveOptions()
        self.PNGSaveOptions = PNGSaveOptions()
        self.Preferences = Preferences()
        self.PresentationOptions = PresentationOptions()
        self.RawFormatOpenOptions = RawFormatOpenOptions()
        self.RawSaveOptions = RawSaveOptions()
        self.RGBColor = RGBColor()
        self.Selection = Selection()
        self.SGIRGBSaveOptions = SGIRGBSaveOptions()
        self.SolidColor = SolidColor()
        self.SubPathInfo = SubPathInfo()
        self.SubPathItem = SubPathItem()
        self.SubPathItems = SubPathItems()
        self.TargaSaveOptions = TargaSaveOptions()
        self.TextFont = TextFont()
        self.TextFonts = TextFonts()
        self.TextItem = TextItem()
        self.TiffSaveOptions = TiffSaveOptions()
        self.XMPMetadata = XMPMetadata()
        self.PsAdjustmentReference = PsAdjustmentReference()
        self.PsAnchorPosition = PsAnchorPosition()
        self.PsAntiAlias = PsAntiAlias()
        self.PsAutoKernType = PsAutoKernType()
        self.PsBatchDestinationType = PsBatchDestinationType()
        self.PsBitmapConversionType = PsBitmapConversionType()
        self.PsBitmapHalfToneType = PsBitmapHalfToneType()
        self.PsBitsPerChannelType = PsBitsPerChannelType()
        self.PsBlendMode = PsBlendMode()
        self.PsBMPDepthType = PsBMPDepthType()
        self.PsByteOrder = PsByteOrder()
        self.PsCameraRAWSettingsType = PsCameraRAWSettingsType()
        self.PsCameraRAWSize = PsCameraRAWSize()
        self.PsCase = PsCase()
        self.PsChangeMode = PsChangeMode()
        self.PsChannelType = PsChannelType()
        self.PsColorBlendMode = PsColorBlendMode()
        self.PsColorModel = PsColorModel()
        self.PsColorPicker = PsColorPicker()
        self.PsColorProfileType = PsColorProfileType()
        self.PsColorReductionType = PsColorReductionType()
        self.PsColorSpaceType = PsColorSpaceType()
        self.PsCopyrightedType = PsCopyrightedType()
        self.PsCreateFields = PsCreateFields()
        self.PsCropToType = PsCropToType()
        self.PsDCSType = PsDCSType()
        self.PsDepthMapSource = PsDepthMapSource()
        self.PsDescValueType = PsDescValueType()
        self.PsDialogModes = PsDialogModes()
        self.PsDirection = PsDirection()
        self.PsDisplacementMapType = PsDisplacementMapType()
        self.PsDitherType = PsDitherType()
        self.PsDocumentFill = PsDocumentFill()
        self.PsDocumentMode = PsDocumentMode()
        self.PsEditLogItemsType = PsEditLogItemsType()
        self.PsElementPlacement = PsElementPlacement()
        self.PsEliminateFields = PsEliminateFields()
        self.PsExportType = PsExportType()
        self.PsExtensionType = PsExtensionType()
        self.PsFileNamingType = PsFileNamingType()
        self.psFontPreviewType = psFontPreviewType()
        self.PsForcedColors = PsForcedColors()
        self.PsFormatOptionsType = PsFormatOptionsType()
        self.PsGalleryConstrainType = PsGalleryConstrainType()
        self.PsGalleryFontType = PsGalleryFontType()
        self.PsGallerySecurityTextPositionType = PsGallerySecurityTextPositionType()
        self.PsGallerySecurityTextRotateType = PsGallerySecurityTextRotateType()
        self.PsGallerySecurityType = PsGallerySecurityType()
        self.PsGalleryThumbSizeType = PsGalleryThumbSizeType()
        self.PsGeometry = PsGeometry()
        self.PsGridLineStyle = PsGridLineStyle()
        self.PsGridSize = PsGridSize()
        self.PsGuideLineStyle = PsGuideLineStyle()
        self.PsIllustratorPathType = PsIllustratorPathType()
        self.PsIntent = PsIntent()
        self.PsJavaScriptExecutionMode = PsJavaScriptExecutionMode()
        self.PsJustification = PsJustification()
        self.PsLanguage = PsLanguage()
        self.PsLayerCompressionType = PsLayerCompressionType()
        self.PsLayerKind = PsLayerKind()
        self.PsLayerType = PsLayerType()
        self.PsLensType = PsLensType()
        self.PsMagnificationType = PsMagnificationType()
        self.PsMatteType = PsMatteType()
        self.PsMeasurementRange = PsMeasurementRange()
        self.PsMeasurementSource = PsMeasurementSource()
        self.PsNewDocumentMode = PsNewDocumentMode()
        self.PsNoiseDistribution = PsNoiseDistribution()
        self.PsOffsetUndefinedAreas = PsOffsetUndefinedAreas()
        self.PsOpenDocumentMode = PsOpenDocumentMode()
        self.PsOpenDocumentType = PsOpenDocumentType()
        self.PsOperatingSystem = PsOperatingSystem()
        self.PsOrientation = PsOrientation()
        self.PsOtherPaintingCursors = PsOtherPaintingCursors()
        self.PsPaintingCursors = PsPaintingCursors()
        self.PsPaletteType = PsPaletteType()
        self.PsPathKind = PsPathKind()
        self.PsPDFCompatibilityType = PsPDFCompatibilityType()
        self.PsPDFEncoding = PsPDFEncoding()
        self.PsPDFResampleType = PsPDFResampleType()
        self.PsPDFStandardType = PsPDFStandardType()
        self.PsPhotoCDColorSpace = PsPhotoCDColorSpace()
        self.PsPhotoCDSize = PsPhotoCDSize()
        self.PsPICTBitsPerPixels = PsPICTBitsPerPixels()
        self.PsPICTCompression = PsPICTCompression()
        self.PsPicturePackageTextType = PsPicturePackageTextType()
        self.PsPointKind = PsPointKind()
        self.PsPointType = PsPointType()
        self.PsPolarConversionType = PsPolarConversionType()
        self.PsPreviewType = PsPreviewType()
        self.PsPurgeTarget = PsPurgeTarget()
        self.PsQueryStateType = PsQueryStateType()
        self.PsRadialBlurMethod = PsRadialBlurMethod()
        self.PsRadialBlurQuality = PsRadialBlurQuality()
        self.PsRasterizeType = PsRasterizeType()
        self.PsReferenceFormType = PsReferenceFormType()
        self.PsResampleMethod = PsResampleMethod()
        self.PsRippleSize = PsRippleSize()
        self.PsSaveBehavior = PsSaveBehavior()
        self.PsSaveDocumentType = PsSaveDocumentType()
        self.PsSaveEncoding = PsSaveEncoding()
        self.PsSaveLogItemsType = PsSaveLogItemsType()
        self.PsSaveOptions = PsSaveOptions()
        self.PsSelectionType = PsSelectionType()
        self.PsShapeOperation = PsShapeOperation()
        self.PsSmartBlurMode = PsSmartBlurMode()
        self.PsSmartBlurQuality = PsSmartBlurQuality()
        self.PsSourceSpaceType = PsSourceSpaceType()
        self.PsSpherizeMode = PsSpherizeMode()
        self.PsStrikeThruType = PsStrikeThruType()
        self.PsStrokeLocation = PsStrokeLocation()
        self.PsTargaBitsPerPixels = PsTargaBitsPerPixels()
        self.PsTextComposer = PsTextComposer()
        self.PsTextType = PsTextType()
        self.PsTextureType = PsTextureType()
        self.PsTIFFEncodingType = PsTIFFEncodingType()
        self.PsToolType = PsToolType()
        self.PsTransitionType = PsTransitionType()
        self.PsTrimType = PsTrimType()
        self.PsTypeUnits = PsTypeUnits()
        self.PsUndefinedAreas = PsUndefinedAreas()
        self.PsUnderlineType = PsUnderlineType()
        self.PsUnits = PsUnits()
        self.PsUrgency = PsUrgency()
        self.PsWarpStyle = PsWarpStyle()
        self.PsWaveType = PsWaveType()
        self.PsWhiteBalanceType = PsWhiteBalanceType()
        self.PsZigZagType = PsZigZagType()
        self.Events = Events()
